# Внутреннее API Wildberries на Python

## Установка

```
pip install -U git+https://gitlab.com/fomch/wildberries_internal_api
```

## Авторизации

```python
from wildberries_internal_api import WBSellerAuth

wb = WBSellerAuth("<phone_number>")
if not wb.check_session():
    print(wb.send_number_phone())
    wb.send_captcha()
    wb.send_verify_code()
```

## Пример использования

```python
from wildberries_internal_api import AnalyticAPI

analytic_api = AnalyticAPI("<phone_number>")
sppls = analytic_api.get_user_suppliers()
print(analytic_api.seller_wb_balance_report(sppls[0]["id"])) 
```
