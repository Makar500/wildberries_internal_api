from .authentication import WBSellerAuth


class SellerInternalAPI(WBSellerAuth):

    def __init__(self, phone_number: str, path_session='sessions/') -> None:
        """	Авторизация/вход в сессию
        :param phone_number: Номер телефона без "+"
        :param path_session: Относительный путь к папке с сессиями
        :raise invalid_phone_number: Некорректный номер телефона
        :raise too_many_attempts: Большое количество попыток. Необходимо подождать.
        """

        super().__init__(phone_number, path_session)

    def get_user_suppliers(self) -> list:
        """ Получить всех поставщиков
        Требуется авторизация
        :return dict: Возвращает данные о доступных поставщиках
        """

        url = "https://seller.wildberries.ru/ns/suppliers/suppliers-portal-core/suppliers"
        body = [{
            "method": "getUserSuppliers",
            "params": {},
            "id": "json-rpc_3",
            "jsonrpc": "2.0"
        }, {
            "method": "listCountries",
            "params": {},
            "id": "json-rpc_4",
            "jsonrpc": "2.0"
        }]
        headers = {
            'Accept': '*/*',
            'Accept-Language': 'ru-RU,ru;q=0.9',
            'Connection': 'keep-alive',
            'Content-type': 'application/json',
            'Origin': 'https://seller.wildberries.ru',
            'Referer': 'https://seller.wildberries.ru/login/ru/?redirect_url=/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }
        response = self.session.post(url, json=body, headers=headers)
        # print('Response', response.status_code)
        response = response.json()
        try:
            return response[0]['result']['suppliers']
        except:
            return response[1]['result']['suppliers']


class AnalyticAPI(SellerInternalAPI):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def consolidated_analytics_back_consolidated_table(self, supplier_id):
        """
        Получить сводную таблицу по продавцу

        https://seller.wildberries.ru/analytics/summary-report
        """

        self.session.cookies.set('x-supplier-id-external', supplier_id, domain='.wildberries.ru')
        self.session.cookies.set('x-supplier-id', supplier_id, domain='.wildberries.ru')

        self.session.cookies.set('locale', 'ru', domain='seller.wildberries.ru')
        self.session.cookies.set('external-locale', 'ru', domain='seller.wildberries.ru')

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
            'Connection': 'keep-alive',
            'Content-type': 'application/json',
            'Referer': 'https://seller.wildberries.ru/analytics/summary-report',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }
        url = 'https://seller.wildberries.ru/ns/consolidated/analytics-back/api/v1/consolidated-table?isCommission=2'
        response = self.session.get(url, headers=headers)
        if not response:
            raise Exception(response.text)
        return response.json()

    def seller_wb_balance_report(self, supplier_id):
        """
        Финансовый отчет

        https://seller.wildberries.ru/suppliers-mutual-settlements/reports-implementations/reports-daily
        """

        self.session.cookies.set('x-supplier-id-external', supplier_id, domain='.wildberries.ru')
        self.session.cookies.set('x-supplier-id', supplier_id, domain='.wildberries.ru')

        self.session.cookies.set('locale', 'ru', domain='seller.wildberries.ru')
        self.session.cookies.set('external-locale', 'ru', domain='seller.wildberries.ru')

        headers = {
            'authority': 'seller-weekly-report.wildberries.ru',
            'accept': '*/*',
            'accept-language': 'ru,en;q=0.9,fr;q=0.8',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not_A Brand";v="8", "Chromium";v="120", "YaBrowser";v="24.1", "Yowser";v="2.5"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 YaBrowser/24.1.0.0 Safari/537.36',
        }
        url = 'https://seller-weekly-report.wildberries.ru/ns/reports/seller-wb-balance/api/v1/reports?dateFrom=&dateTo=&limit=31&searchBy=&skip=0&type=5'
        response = self.session.get(url, headers=headers)
        if not response:
            raise Exception(response.text)
        return response.json()

    def content_analytics_report_table_detail(self, start_date, end_date, limit=50, offset=0):
        """
        Получить данные аналитики по товарам из ЛК

        https://seller.wildberries.ru/content-analytics/interactive-report
        """

        body = {
            "subjects": [],
            "brands": [],
            "tagIds": [],
            "nms": [],
            "start": start_date,
            "end": end_date,
            "limit": limit,
            "offset": offset,
            "orderBy": {
                "field": "orders",
                "mode": "desc"
            },
            "skipDeletedNm": False
        }
        headers = {
            'accept': '*/*',
            'accept-language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
        }
        url = 'https://seller-content.wildberries.ru/ns/analytics-api/content-analytics/api/v1/report/table-detail'
        response = self.session.post(url, json=body, headers=headers)
        return response.json()


class CPMAPI:

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def cpm_api_v5_upd(self, supplier_id, pageNumber=1, pageSize=50):
        """
        Получить данные о финансах

        https://cmp.wildberries.ru/campaigns/finances
        """

        self.session.cookies.set("x-supplier-id", supplier_id, domain="seller.wildberries.ru")
        self.session.cookies.set("x-supplierid", supplier_id, domain="seller.wildberries.ru")

        headers = {
            'accept': '*/*',
            'accept-language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
            'referer': 'https://cmp.wildberries.ru/campaigns/finances',
            'sec-ch-ua': '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
        }
        url = f'https://cmp.wildberries.ru/api/v5/upd?pageNumber={pageNumber}&pageSize={pageSize}&type=%5B4%2C5%2C6%2C7%2C8%2C9%5D'
        response = self.session.get(url, headers=headers)
        return response.json()
