import os
import pickle
from uuid import uuid4

import requests


class WBSellerAuth:

    def __init__(self, phone_number, session_folder='wb_sessions') -> None:
        self.phone_number = phone_number
        self.session_path = os.path.join(session_folder, f"{phone_number}.session")

        if not os.path.exists(session_folder):
            os.makedirs(session_folder)

        self.session = requests.Session()
        if not os.path.exists(self.session_path):
            self._save_session()

        self._load_session()

        # TODO получать внешний IP нормальным способом
        response_ip = str(requests.get("https://api.ipify.org/?format=json").json()['ip'])

        self.device_id = str(uuid4())
        self.headers = {
            'Host': 'seller-auth.wildberries.ru',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:125.0) Gecko/20100101 Firefox/125.0',
            'Accept': '*/*',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Referer': 'https://seller-auth.wildberries.ru/ru/?redirect_url=https://seller.wildberries.ru/?skipLanding',
            'Content-type': 'application/json',
            'deviceId': f'supplier-portal__-{self.device_id}',
            'X-Real-IP': response_ip,
            'Origin': 'https://seller-auth.wildberries.ru',
            'DNT': '1',
            'Sec-GPC': '1',
            'Connection': 'keep-alive',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'Content-Length': '0',
            'TE': 'trailers'
        }

    def check_session(self):
        response = self._make_request(url="https://seller-auth.wildberries.ru/auth/v2/auth/slide-v3")
        return str(response.get('result')) == "0"
        
    def _load_session(self):
        with open(self.session_path, 'rb') as f:
            self.session = pickle.load(f)

    def _save_session(self):
        with open(self.session_path, 'wb') as f:
            pickle.dump(self.session, f)
    
    def _make_request(self, method="POST", json_data={}, url=None):

        if method == 'POST':
            r = self.session.post(url, json=json_data, headers=self.headers)
        if method == 'GET':
            r = self.session.get(url, params=json_data, headers=self.headers)
        
        self._save_session()

        return r.json()
    
    def _upgrade_deviceid(self):
        url = "https://seller-auth.wildberries.ru/upgrade-deviceid-authv3"
        self._make_request(url=url)

    def send_number_phone(self):
        self._upgrade_deviceid()

        url = "https://seller-auth.wildberries.ru/auth/v2/code"
        data = {"phone_number":self.phone_number,"captcha_code":""}

        response = self._make_request(json_data=data, url=url)

        if str(response.get('result')) == '4':
            raise Exception(f"Ожидание капчи: {response.get('payload', {}).get('ttl')} сек.")
        
        if str(response.get('result')) == '3':
            return response.get('payload').get('captcha')
        
        return None
    
    def send_captcha(self, captcha_code=None):
        url = "https://seller-auth.wildberries.ru/auth/v2/code"
        captcha_code = captcha_code or input("Введите капчу: ")
        data = {"phone_number":self.phone_number,"captcha_code":str(captcha_code)}

        response = self._make_request(json_data=data, url=url)

        print(response)

        if response.get('result') != 0:
            raise Exception(str(response))
        
        self.sticker = response['payload']['sticker']
        
        return None

    def send_verify_code(self, verify_code=None):
        verify_code = verify_code or input("Введите код: ")
        url = "https://seller-auth.wildberries.ru/auth/v2/auth"
        data = {"sticker":self.sticker,"code":int(verify_code)}

        response = self._make_request(json_data=data, url=url)
        print(response)
        if response.get('result') == 6:
            raise Exception("Неверный код")

        return response
